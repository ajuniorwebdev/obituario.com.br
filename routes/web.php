<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::redirect('/register', '/login');
Route::redirect('/password/reset', '/login');
Route::redirect('password/reset/{token}', '/login');
Route::redirect('email/verify', '/login');
Route::redirect('email/verify/{id}', '/login');
Route::redirect('email/resend', '/login');

Route::get('/', 'ObituarioController@index')->name('obituario');
Route::get('/home', 'ObituarioController@index')->name('obituario');
Route::post('/search', 'ObituarioController@search')->name('search');

Route::group(['middleware' => 'auth'], function() {
    
    Route::get('/create', 'ObituarioController@create')->name('create');
    Route::post('/store', 'ObituarioController@store')->name('store');
    Route::get('/edit/{id}', 'ObituarioController@edit')->name('edit');
    Route::post('/update/{id}', 'ObituarioController@update')->name('update');
    Route::post('/destroy/{id}', 'ObituarioController@destroy')->name('destroy');
    
    Route::get('/usuario', 'UsuariosController@index')->name('usuario');
    Route::get('usuario/show/{id}', 'UsuariosController@show')->name('usuario.show');
    Route::get('/meusdados', 'UsuariosController@showWithAuth')->name('usuario.auth');
    Route::post('/usuario/create', 'UsuariosController@create')->name('usuario.create');
    Route::get('/usuario/edit/{id}', 'UsuariosController@edit')->name('usuario.edit');
    Route::post('/usuario/update/{id}', 'UsuariosController@update')->name('usuario.update');
    Route::post('/usuario/destroy/{id}', 'UsuariosController@destroy')->name('usuario.destroy');
    Route::get('/usuario/pass', 'UsuariosController@changePass')->name('usuario.pass');
   
});

