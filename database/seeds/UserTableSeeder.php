<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
        $id = DB::table('users')
            ->insertGetId(['name' => 'Ailton Junior', 'email' => 'a.juniorwebdev@gmail.com', 'password' => Hash::make('ju03ni08'), 'created_at' => now()]);
    
        $user = User::find($id);
        $user->assignRole('adm');
        unset($user, $id);
    
        $id = DB::table('users')
            ->insertGetId(['name' => 'Usuário Consulta', 'email' => 'email@obituario.com.br', 'password' => Hash::make('102030@obituario'), 'created_at' => now()]);
    
        $user = User::find($id);
        $user->assignRole('user');
        unset($user, $id);
    }
}
