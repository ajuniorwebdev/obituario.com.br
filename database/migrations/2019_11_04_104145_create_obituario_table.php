<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObituarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('obituario', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome', 255);
            $table->date('data');
            $table->string('lema', 100);
            $table->string('path_image', 255);
            $table->string('path_doc', 255);
            $table->text('search_obituario');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('obituario');
    }
}
