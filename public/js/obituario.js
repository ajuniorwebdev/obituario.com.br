$(document).ready(() => {

    $('input[name="data"]').mask('00/00/0000')

    $('a.search').on('click', (e) => {

        $.ajax({
            url: '/search',
            method: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                text: ($('input#search_input').val().length == 0) ? null : $('input#search_input').val()
            },
            success: (res) => {
                $('div.pesquisa').css('display', 'block')
                $('#resultado').html('').html(res.html)

                $('a.btn.btn-danger.open-modal').on('click', (e) => {
                    $('div.modal-remove a.remove').attr('data-id', $(e.target).attr('data-id'))
                    $('div.modal-remove').modal('show')
                })

            },
            error: (err) => {
                console.log(err)
            }
        })

    })

    $('input#search_input').on('keypress', (e) => {
        if (e.keyCode == 13) {
            e.preventDefault()
            $('a.search').trigger('click')
        }
    })

    $('input[name="imagem"]').on('change', (e) => {
        let reader = new FileReader()

        reader.onload = (e) => {
            $('img.img').attr('src', e.target.result)
            $('input[name="imagem"]').parent().find('label.custom-file-label').text($('input[name="imagem"]')[0].files[0].name)
        }

        reader.readAsDataURL($('input[name="imagem"]')[0].files[0])
    })

    $('input[name="doc"]').on('change', (e) => {
        let reader = new FileReader()

        reader.onload = (e) => {
            $('input[name="doc"]').parent().find('label.custom-file-label').text($('input[name="doc"]')[0].files[0].name)
        }

        reader.readAsDataURL($('input[name="doc"]')[0].files[0])
    })

    $('a.remove').on('click', (e) => {

        $('div.modal-remove').modal('hide')

        $.ajax({
            url: '/destroy/' + $(e.target).attr('data-id'),
            method: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: (res) => {
                $('a.search').trigger('click')
            },
            error: (err) => {
                console.log(err)
            }
        })
    })

})
