<?php

namespace App\Http\Controllers;

use App\User;
use Spatie\Permission\Models\Role;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        
        return view('usuarios.index', compact('users'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
        $user = User::where(['id' => $id])->first();
        $user_roles = $user->getRoleNames();
        $role = null;
    
        switch ($user_roles[0]) {
            case 'adm':
                $role = 'Administrador';
                break;
        
            case 'user':
                $role = 'Usuário';
                break;
        }
    
        return view('usuarios.show', compact(['user', 'role']));
    }
    
    /**
     * Display the user with has Authentication.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function showWithAuth()
    {
        $id = Auth::id();
        
        $user = User::where(['id' => $id])->first();
        $user_roles = $user->getRoleNames();
        $role = null;
        
        switch ($user_roles[0]) {
            case 'adm':
                $role = 'Administrador';
                break;
            
            case 'user':
                $role = 'Usuário';
                break;
        }
        
        return view('usuarios.show_auth', compact(['user', 'role']));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $roles = Role::all();
        $user_roles = [];
        
        foreach ($roles as $r) {
            switch ($r->name) {
                case 'adm':
                    array_push($user_roles, [
                        'id' => $r->id,
                        'name' => $r->name,
                        'pseudo' => 'Administrador'
                    ]);
                    break;
                
                case 'user':
                    array_push($user_roles, [
                        'id' => $r->id,
                        'name' => $r->name,
                        'pseudo' => 'Usuário'
                    ]);
                    break;
            }
        }
        
        $user = User::where(['id' => $id])->first();
        $user_role = $user->getRoleNames()->first();
        
        return view('usuarios.edit', compact(['user', 'user_roles', 'user_role']));
        
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
        $validate = [
            'nome' => 'required',
            'roles' => 'required'
        ];
    
        if($request->filled('password')) {
            $validate['password'] = 'required|confirmed';
        }
        
        $this->validate($request, $validate, [
            'required' => 'O campo :attribute é obrigatório',
            'confirmed' => 'Senhas não conferem'
        ]);
        
        try {
            
            $data = [
                'name' => $request->input('nome'),
            ];
    
            if($request->filled('password')) {
                $data['password'] = Hash::make($request->input('password'));
            }
            
            if(User::where(['id' => $id])->update($data)) {
                $user = User::where(['id' => $id])->first();
                if($user->getRoleNames()->first() <> $request->input('roles')) {
                    $user->assignRole($request->input('roles'));
                }
                
                return redirect()
                    ->route('usuario')
                    ->with(['success' => true, 'msg' => 'Usuário alterado com sucesso!']);
            }
            
        } catch(Exception $e) {
            report($e);
            
            return redirect()
                ->route('usuario')
                ->with(['success' => false, 'msg' => 'Ocorreu um problema para alterar o usuário, contate o administrador!']);
        }
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
