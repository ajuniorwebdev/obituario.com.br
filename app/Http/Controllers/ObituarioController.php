<?php

namespace App\Http\Controllers;

use App\Obituario;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ObituarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('obituario/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('obituario/create');
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required',
            'data' => 'required',
            'lema' => 'required',
            'doc' => 'required|mimes:pdf'
        ], [
            'required' => 'Campo :attribute é obrigatório'
        ]);
        
        try {
            
            $data = [
                'nome' => $request->input('nome'),
                'data' => date('Y-m-d', strtotime($request->input('data'))),
                'lema' => $request->input('lema'),
                'search_obituario' => $request->input('nome'). ' ' . $request->input('data') . ' ' . $request->input('lema')
            ];
    
            if($request->hasFile('imagem')) {
                $name = $request->file('imagem')->getClientOriginalName();
                
                $data['path_image'] = $name;
    
                Storage::disk('public_img')->put($name, file_get_contents($request->file('imagem')));
                
            } else {
                
                $data['path_image'] = 'freira1.jpg';
                
            }
    
            if($request->hasFile('doc')) {
                $name = $request->file('doc')->getClientOriginalName();
                $data['path_doc'] = $name;
    
                Storage::disk('public_doc')->put($name, file_get_contents($request->file('doc')));

            }
    
            Obituario::create($data);

            return redirect()
                ->route('obituario')
                ->with('status', ['success' => true, 'msg' => 'Informações cadastradas com sucesso!']);

        } catch(Exception $e) {
            report($e);

            return redirect('create')
                ->with('status', ['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        try {
            $f = Obituario::where(['id' => $id])->first();
    
            return view('obituario.edit', compact('f'));
            
        } catch(Exception $e) {
            
            report($e);
            
            return redirect()
                ->route('obituario')
                ->with('status', ['success' => false, 'msg' => $e->getMessage()]);
            
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nome' => 'required',
            'data' => 'required',
            'lema' => 'required'
        ], [
            'required' => 'Campo :attribute é obrigatório'
        ]);
    
        try {
        
            $data = [
                'nome' => $request->input('nome'),
                'data' => date('Y-m-d', strtotime($request->input('data'))),
                'lema' => $request->input('lema'),
                'search_obituario' => $request->input('nome'). ' ' . $request->input('data') . ' ' . $request->input('lema')
            ];
        
            if($request->hasFile('imagem')) {
                $name = $request->file('imagem')->getClientOriginalName();
            
                $data['path_image'] = $name;
            
                Storage::disk('public_img')->put($name, file_get_contents($request->file('imagem')));
            
            }
        
            if($request->hasFile('doc')) {
                $name = $request->file('doc')->getClientOriginalName();
                $data['path_doc'] = $name;
            
                Storage::disk('public_doc')->put($name, file_get_contents($request->file('doc')));
            
            }
        
            Obituario::where(['id' => $id])
                ->update($data);
        
            return redirect()
                ->route('obituario')
                ->with('status', ['success' => true, 'msg' => 'Informações alteradas com sucesso!']);
        
        } catch(Exception $e) {
            report($e);
        
            return redirect()
                ->route('obituario')
                ->with('status', ['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    
        try {
        
            if(Obituario::destroy($id)) {
                return response()
                    ->json(['success' => true], 200);
            }
            
        } catch (Exception $e) {
            report($e);
    
            return response()
                ->json(['html' => view('obituario.components.search')->with(['list' => false, 'msg' => 'Ocorreu um erro para remover obituario!'])->render()], 200);
            
        }
    
    }
    
    public function search(Request $request)
    {
    
        try {
    
            if(!$request->filled('text')) {
                
                $list = DB::select("SELECT * FROM obituario");
                
            } else {
                
                $text = $request->input('text');
                $list = DB::select("SELECT * FROM obituario WHERE MATCH(search_obituario) AGAINST('$text' IN NATURAL LANGUAGE MODE)");
                
            }
    
            return response()
                ->json(['html' => view('obituario.components.search')->with('list', $list)->render()], 200);
            
        } catch(Exception $e) {
            report($e);
    
            return redirect('/')
                ->with('status', ['success' => false, 'msg' => $e->getMessage()]);
            
        }
    
    }
}
