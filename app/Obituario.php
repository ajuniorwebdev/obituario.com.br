<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Obituario extends Model
{
    
    protected $fillable = [
        'nome', 'data', 'lema', 'path_image', 'path_doc', 'search_obituario', 'created_at', 'updated_at'
    ];
    
    protected $table = 'obituario';
    
}
