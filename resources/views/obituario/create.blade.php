@extends('layouts/app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Cadastrar novo obituário</div>
                    <div class="card-body">
                        <form action="{{ route('store') }}" method="post" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                    <label for="nome">Nome</label>
                                    <input type="text" class="form-control @error('nome') is-invalid @enderror" id="nome" name="nome">
                                    @error('nome')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                    <label for="data">Data do Óbito</label>
                                    <input type="text" class="form-control @error('data') is-invalid @enderror" id="data" name="data">
                                    @error('data')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div> 
                                <div class="col-8 col-sm-8 col-md-8 col-lg-8">
                                    <label for="lema">Lema</label>
                                    <input type="text" class="form-control @error('lema') is-invalid @enderror" id="lema" name="lema">
                                    @error('lema')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                                    <img class="img" src="{{ asset('/storage/img/not_found_img.png') }}" alt="Imagem">
                                </div>
                                <div class="col-10 col-sm-10 col-md-10 col-lg-10">
                                    <label for="imagem">Imagem</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input @error('imagem') is-invalid @enderror" id="imagem" name="imagem">
                                        <label class="custom-file-label" for="imagem">Selecione uma imagem</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                    <label for="doc">Documento Oficial</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input @error('doc') is-invalid @enderror" id="doc" name="doc">
                                        <label class="custom-file-label" for="doc">Selecione o documento oficial</label>
                                    </div>
                                    @error('doc')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="botoes float-right">
                                        <a href="{{ route('obituario') }}" class="btn btn-dark">Cancelar</a>
                                        <button type="submit" class="btn btn-success">Salvar</button>
                                    </div>
                                </div>
                            </div>

                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(session('status'))
        <div class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">@if(session('status')['success']) Sucesso @else Atenção @endif</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>{{ session('status')['msg'] }}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('js')
    <script src="{{ asset('js/obituario.js') }}?v=@php echo date('YmdHis') @endphp"></script>

    @if(session('status'))
        <script>
            $('.modal').modal();
        </script>
    @endif

    
@endsection


