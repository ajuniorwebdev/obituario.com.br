@extends('layouts/app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <form action="">
                            @csrf
                            <div class="form-group row">
                                <div class="col-10 col-sm-10 col-md-10 col-lg-10">
                                    <input class="form-control @error('nome') is-invalid @enderror" type="text" id="search_input" name="search_input" placeholder="Pesquise por nome, lema ou data" autocomplete="off">
                                </div>
                                <div class="col-2 col-sm-2 col-md-2 col-lg-2 d-flex align-items-end">
                                    <a class="btn btn-dark search" href="javascript:void(0)"><i class="fas fa-search mr-2"></i>Pesquisar</a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center mt-4 pesquisa">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div id="resultado"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @if(session('status'))
        <div class="modal modal-alert" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">@if(session('status')['success']) Sucesso @else Atenção @endif</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>{{ session('status')['msg'] }}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="modal modal-remove" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Atenção</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Tem certeza que deseja remover este obituário?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
                    <a href="javascript:void(0)" class="btn btn-danger remove">Remover</a>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('css')
@endsection

@section('js')
    <script src="{{ asset('js/obituario.js') }}?v=@php echo date('YmdHis') @endphp"></script>

    @if(session('status'))
        <script>

            $('.modal.modal-alert').modal()

        </script>
    @endif
@endsection
