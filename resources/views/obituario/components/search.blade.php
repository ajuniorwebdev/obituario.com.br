@if(!$list)
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 d-flex align-items-center">
                    <div class="info mx-auto">
                        <p><i>{{ $msg }}</i></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Imagem</th>
                        <th>Informações</th>
                        <th>Açoes</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($list as $f)
                        <tr>
                            <td><img src="{{ asset('/storage/img/'.$f->path_image) }}" alt="" width="100%"></td>
                            <td>
                                <div class="info">
                                    <h3>{{ $f->nome }}</h3>
                                    <span><b><i>{{ $f->lema }}</i></b></span>
                                    <span>Data: {{ date('m/d/Y', strtotime($f->data)) }}</span>
                                </div>
                            </td>
                            <td>
                                <div class="botoes">
                                    <a class="btn btn-primary" href="{{ asset('/storage/doc/'.$f->path_doc) }}" target="_blank"><i class="fas fa-eye"></i></a>
                                    @auth
                                        <a class="btn btn-warning text-white" href="{{ route('edit', ['id' => $f->id]) }}"><i class="far fa-edit"></i></a>
                                        <a class="btn btn-danger open-modal" href="javascript:void(0)" data-id="{{ $f->id }}"><i class="fas fa-trash-alt"></i></a>
                                    @endauth
                                </div>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endif