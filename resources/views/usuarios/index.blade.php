@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-10 col-sm-10 col-md-10 col-lg-10">
                <div class="card">
                    <div class="card-header">Usuários</div>
                    <div class="card-body">
                        <div class="table-responsive users">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nome</th>
                                    <th>E-mail</th>
                                    <th>Data Cadastro</th>
                                    <th>Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $u)
                                    <tr>
                                        <td>{{ $u->id }}</td>
                                        <td>{{ $u->name }}</td>
                                        <td>{{ $u->email }}</td>
                                        <td>{{ $u->created_at->format('d/m/Y H:i:s') }}</td>
                                        <td>
                                            <div class="botoes">
                                                <a href="{{ route('usuario.show', ['id' => $u->id]) }}" class="btn btn-primary"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('usuario.edit', ['id' => $u->id]) }}" class="btn btn-warning"><i class="far fa-edit"></i></a>
                                                <a href="{{ route('usuario.destroy', ['id' => $u->id]) }}" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
                                            </div>
                                        </td>
                                    </tr>    
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(session('status'))
        <div class="modal modal-alert" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">@if(session('status')['success']) Sucesso @else Atenção @endif</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>{{ session('status')['msg'] }}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection