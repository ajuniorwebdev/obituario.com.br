@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Alterar informações de usuário</div>
                    <div class="card-body">
                        <form action="{{ route('usuario.update', ['id' => $user->id]) }}" method="post">
                            @csrf
                            <div class="form-group row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                    <label for="nome">Nome</label>
                                    <input type="text" class="form-control @error('nome') @enderror" name="nome" id="nome" value="{{ $user->name ?? old('nome') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                    <label for="email">E-mail</label>
                                    <input type="text" class="form-control" id="email" value="{{ $user->email }}" disabled>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                    <label for="roles">Permissões</label>
                                    <select class="form-control" name="roles" id="roles">
                                        <option value="">Selecione um opção</option>
                                        @foreach($user_roles as $r)
                                            <option value="{{ $r['name'] }}" {{ ($r['name'] == $user_role) ? 'selected' : '' }}>{{ $r['pseudo'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                    <label for="password">Senha</label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="off">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                    <label for="password">Confirmar Senha</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="off">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="botoes float-right">
                                        <a href="{{ route('usuario') }}" class="btn btn-dark">Cancelar</a>
                                        <button type="submit" class="btn btn-success">Salvar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection