@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="card-header">Meus Dados</div>
                        <div class="card-body">
                            <form action="">
                                {{--div.col-12.col-sm-12.col-md-12.col-lg-12--}}
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                        <label for=""><b>Nome</b></label>
                                        <p>{{ $user->name }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                        <label for=""><b>E-mail</b></label>
                                        <p>{{ $user->email }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                        <label for=""><b>Permissão</b></label>
                                        <p>{{ $role }}</p>
                                    </div>

                                    <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                        <label for=""><b>Data de cadastro</b></label>
                                        <p>{{ $user->created_at->format('d/m/Y H:i:s') }}</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="botoes float-right">
                                            <a class="btn btn-dark" href="{{ route('usuario') }}">Voltar</a>
                                            <a class="btn btn-warning" href="{{ route('usuario.edit', ['id' => $user->id]) }}">Alterar Informações</a>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection